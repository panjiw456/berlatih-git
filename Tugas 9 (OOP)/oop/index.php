<?php

require_once('animal.php');
require_once('ape.php');
require_once('frog.php');

 $sheep = new animal('Shaun');

 echo "Name = " .  $sheep->nama . "<br>";
 echo "Leg = " .  $sheep->leg . "<br>";
 echo "Cold Blooded = " .  $sheep->coldBlooded . "<br><br>";

 $sungokong = new ape("Kera Sakti");

 echo "Name = " .  $sungokong->nama . "<br>";
 echo "Leg = " .  $sungokong->leg . "<br>";
 echo "Cold Blooded = " .  $sungokong->coldBlooded . "<br>";
 echo "Yell = " . $sungokong->yell() . "<br><br>" ;

 $kodok = new frog("Buduk");

 echo "Name = " .  $kodok->nama . "<br>";
 echo "Leg = " .  $kodok->leg . "<br>";
 echo "Cold Blooded = " .  $kodok->coldBlooded . "<br>";
 echo "Jump = " . $kodok->jump() . "<br><br>" ;

 
?>